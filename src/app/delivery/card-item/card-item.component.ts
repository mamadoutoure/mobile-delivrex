import {Component, Input, OnInit} from '@angular/core';
import {IDelivery} from '../../models/delivery.model';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
})
export class CardItemComponent implements OnInit {
@Input()
delivery: IDelivery;
  constructor() { }

  ngOnInit() {}

}
