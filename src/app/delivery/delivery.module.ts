import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DeliveryPage } from './delivery.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { DeliveryPageRoutingModule } from './delivery-routing.module';
import {CardItemComponent} from './card-item/card-item.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    DeliveryPageRoutingModule
  ],
  declarations: [DeliveryPage, CardItemComponent]
})
export class DeliveryPageModule {}
