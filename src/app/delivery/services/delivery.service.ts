import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {IDelivery} from '../../models/delivery.model';
import {IOrder} from '../../models/order.model';
import {ICompany} from '../../models/company.model';
import {CompanyType, ContactType} from '../../models/entity-types.model';
import {ICustomer} from '../../models/customer.model';
import {ILocation} from '../../models/location.model';
import {IContact} from '../../models/contact.model';
import {IItem} from '../../models/item.model';
import {DeliveryStatus, OrderStatus} from '../../models/status.model';
import {IShipper} from '../../models/shipper.model';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

        constructor() { }
        getDeliveries(): Observable<IDelivery[]>{



            const mockDeliveries: IDelivery[] = [

                {id: 1, dropLocation: [10, 30], dropTimestamp: Date.now(), pickupLocation: [34, 46],
                 pickupTimestamp: Date.now(), order: this.mockOrder(), shipper: this.mockShipper(),
                    status: [{status: DeliveryStatus.DELIVERED, time: Date.now()}]
                },
                {id: 3, dropLocation: [1033, 340], dropTimestamp: Date.now(), pickupLocation: [345, 9846],
                    pickupTimestamp: Date.now(), order: this.mockOrder(), shipper: this.mockShipper(),
                    status: [{status: DeliveryStatus.IN_PROGRESS, time: Date.now()}]
                },
                {id: 4, dropLocation: [10556, 35650], dropTimestamp: Date.now(), pickupLocation: [3544, 46546],
                    pickupTimestamp: Date.now(), order: this.mockOrder(), shipper: this.mockShipper(),
                    status: [{status: DeliveryStatus.CANCELLED, time: Date.now()}]
                }

            ];
            return of(mockDeliveries);
        }
        private mockCustomer(): ICustomer{
          const customer: ICustomer = {id: Math.random(),
              firstName: 'Client ',
              lastName: '' + Math.random(),
              contact: [this.mockContact(1), this.mockContact(2), this.mockContact(3)],
              address: this.mockLocation()};
          return customer;
        }
        private  mockLocation(): ILocation{
          return  {address: 'Parcelle Assainies U' +  Math.round(Math.random() * 100) + ' Villa N. ' +  Math.round(Math.random() * 10000),
              latLon: this.mockLatLon()};
        }
        private  mockLatLon(): number[]{
            return   [ Math.round(Math.random() * 100000),  Math.round(Math.random() * 100000)];
        }

        private  mockItem(): IItem{
            return {name: 'Produit-' +  Math.round(Math.random() * 1000), price:  Math.round(Math.random() * 1000)};
        }

        private mockContact(type: number): IContact {
            return {phone: '(' +  Math.round(Math.random() * 1000) + ')' + Math.round(Math.random() * 1000000),
                type: type = 1 ? ContactType.CELL : type = 2 ? ContactType.FIX : ContactType.SKYPE};
        }

        private mockCompany(): ICompany {
            return {address: this.mockLocation(), code: 'CER', contact: 'Youssouf Mbaye', id: 0,
                name: 'Chez Khady', type: CompanyType.SHOP};
        }

        private mockOrder(): IOrder {
            return {company: this.mockCompany(), companyId: 0, customerId: 0, date: new Date(),
                destinationAddress: this.mockLocation(), id: 0,
                customer: this.mockCustomer(), items: [this.mockItem()], status: [{status: OrderStatus.NEW, time: Date.now()}]

            };
        }

        private mockShipper(): IShipper {
            return {lastName: 'last-' +  Math.round(Math.random() * 1000),
                firstName: 'first-' +  Math.round(Math.random() * 1000),
                address: this.mockLocation(),
                companyId: 1, contact: [this.mockContact(2)] , id: 3};
        }
}
