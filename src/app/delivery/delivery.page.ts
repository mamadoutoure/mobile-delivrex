import {Component, OnInit} from '@angular/core';
import {DeliveryService} from './services/delivery.service';
import {Observable, of} from 'rxjs';
import {IDelivery} from '../models/delivery.model';
import {catchError} from 'rxjs/operators';

@Component({
  selector: 'app-delivery',
  templateUrl: 'delivery.page.html',
  styleUrls: ['delivery.page.scss']
})
export class DeliveryPage  implements OnInit{

  deliveries$: Observable<IDelivery[]>;
  constructor(private deliveryService: DeliveryService) {}



    ngOnInit(): void {
     this.deliveries$ = this.deliveryService.getDeliveries().pipe(
         catchError((err) => {
           console.log(err);
           return of(err);
         })
     );
    }


}
