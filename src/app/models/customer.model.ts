import {IContact} from './contact.model';
import {ILocation} from './location.model';
import {IPerson} from './person.model';

export interface ICustomer extends IPerson {
    id: number;
    contact: IContact[];
    address: ILocation;
}
