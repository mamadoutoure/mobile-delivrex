export interface ILocation {
    address: string;
    latLon: number[];
}
