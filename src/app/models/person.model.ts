import {IContact} from './contact.model';
import {ILocation} from './location.model';

export interface IPerson {
    id: number;
    firstName: string;
    lastName: string;
}
