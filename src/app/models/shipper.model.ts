import {IItem} from './item.model';
import {ICustomer} from './customer.model';
import {IStatus} from './status.model';
import {IPerson} from './person.model';
import {IContact} from './contact.model';
import {ILocation} from './location.model';

export interface IShipper extends IPerson{
    id: number;
    companyId: number;
    contact: IContact[];
    address: ILocation;

}
