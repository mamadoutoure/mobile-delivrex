
export enum CompanyType {
    // 1 = Livraison, 2 = Boutique,  3 = honored -1 = cancelled
    SHIPPING,
    SHOP,
}

export enum ContactType {
    CELL,
    WHATSAPP,
    FIX,
    SKYPE,
    EMAIL,

}
