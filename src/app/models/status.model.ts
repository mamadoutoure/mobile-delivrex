import {IItem} from './item.model';
import {ICustomer} from './customer.model';

export interface IStatus {
    status: number;
    time: number;

}

export enum DeliveryStatus {
    // 1 = en cours, 2 = livre  3 = cancelled
    IN_PROGRESS ,
    DELIVERED ,
    CANCELLED,
}

export enum OrderStatus {
    // 1 = new, 2 = assigned,  3 = honored -1 = cancelled
    NEW ,
    ASSIGNED ,
    HONORED ,
    CANCELLED
}


