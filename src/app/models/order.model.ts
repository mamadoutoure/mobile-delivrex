import {IItem} from './item.model';
import {ICustomer} from './customer.model';
import {IStatus} from './status.model';
import {ICompany} from './company.model';
import {ILocation} from './location.model';

export interface IOrder {
    id: number;
    customerId: number;
    customer: ICustomer;
    companyId: number;
    company: ICompany;
    destinationAddress: ILocation;
    date: Date;
    items: IItem[];
    status: IStatus[]; // 1 = new, 2 = assigned,  3 = honored -1 = cancelled

}
