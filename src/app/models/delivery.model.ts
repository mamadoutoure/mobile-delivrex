import {ICustomer} from './customer.model';
import {IOrder} from './order.model';
import {IStatus} from './status.model';
import {IShipper} from './shipper.model';

export interface IDelivery {
    id: number;
    order: IOrder;
    shipper: IShipper;
    pickupTimestamp: number;
    dropTimestamp: number;
    pickupLocation: number[];
    dropLocation: number[];
    status: IStatus[]; // 1 = en cours, 2 = livre  3 = cancelled

}
