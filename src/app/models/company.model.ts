import {CompanyType} from './entity-types.model';
import {ILocation} from './location.model';

export interface ICompany {
    id: number;
    code: string;
    name: string;
    address: ILocation;
    contact: string;
    type: CompanyType;

}
