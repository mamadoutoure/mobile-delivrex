import {ContactType} from './entity-types.model';

export interface IContact {
    phone: string;
    type: ContactType;
}
